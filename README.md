# :vertical_traffic_light: Tic Tac Toe :vertical_traffic_light:

The game panel contains clickable boxes in 3×3 grid view. On clicking these boxes, it will be marked with the corresponding markers. When the user selecting a box then the X-marker will be added and a random box will be chosen to add O-marker for the computer’s turn.

#### HTML Code for Tic Tac Toe Game Panel

This HTML code is used to show the 3×3 grid view as the Tic Tac Toe game panel. This grid contains 9 clickable boxes. On clicking these boxes, the jQuery function will be called to mark the users play and trigger computer’s play automatically.

#### jQuery Code to Mark the User and Computer Entry

The following jQuery script contains functions that are used to handle the click event of the users and to trigger the computer’s play. When the user selecting a box in the game panel, then the jQuery code will be executed to mark that box with X-marker. Then, it will choose a random box from the game panel which is not clicked and marked already. This random box will be marked with the O-marker for the computer’s turn.

:x: :o:

